# Proximity Sensor

## Description
Circuit Schematics and Arduino code to operate an ITR20001\T infrared proximity sensor.

## License
MIT License.
