#define ANALOG_IN A3
#define DIGITAL_OUT 4

int analogValue1 = 0;
int analogValue2 = 0;
void setup() {
  // put your setup code here, to run once:
  pinMode(DIGITAL_OUT, OUTPUT);
}

void loop() {
  // put your main code here, to run repeatedly:
  analogValue1 = analogRead(ANALOG_IN);
  delay(50);
  analogValue2 = analogRead(ANALOG_IN);
  if((analogValue1 > 100 || analogValue2 > 100) && abs(analogValue2-analogValue1) < 50)
  {
    digitalWrite(DIGITAL_OUT, HIGH);
  }
  else
  {
    digitalWrite(DIGITAL_OUT, LOW);
  }
}
